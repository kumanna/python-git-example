import math
def factorial(n):
    """
    Returns the factorial of `n`

    EXAMPLES
    >>> factorial(0)
    1
    >>> factorial(4)
    24
    >>> factorial(5)
    120
    """
    if n < 0: return 0
    if n == 0: return 1
    product = 1
    for i in range(1, n + 1):
        product = product * i;
    return product

def stirling_approx(n):
    return math.sqrt(2 * math.pi * n) * math.pow(n / math.e, n)

def fib(n):
    """
    Returns the nth Fibonacci number, where n >= 1.

    EXAMPLES:
    >>> fib(1)
    0
    >>> fib(2)
    1
    >>> fib(3)
    1
    >>> fib(4)
    2
    >>> fib(5)
    3
    >>> fib(6)
    5
    >>> fib(7)
    8
    """
    if n <= 1:
        return 0
    a = 0
    b = 1
    i = 2
    while i <= n:
        a, b = b, a + b
        i = i + 1
    return a


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)
